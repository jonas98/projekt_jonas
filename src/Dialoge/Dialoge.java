package Dialoge;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Dialoge extends JFrame {

	private JPanel contentPane;

	public Dialoge() {
		super("Dialoge");
		createGUI();
	}

	private void createGUI() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 351, 340);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnAufg1 = new JButton("Aufgabe 1: Zahl zwischen 1 und 100?");
		btnAufg1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				aufg1();
			}
		});
		btnAufg1.setBounds(12, 12, 320, 25);
		contentPane.add(btnAufg1);

		JButton btnAufg2 = new JButton("Aufgabe 2: Zahl kleiner als 10 oder größer als 20?");
		btnAufg2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				aufg2();
			}
		});
		btnAufg2.setBounds(12, 49, 320, 25);
		contentPane.add(btnAufg2);

		JButton btnAufg3 = new JButton("Aufgabe 3: Gerade Zahl?");
		btnAufg3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				aufg3();
			}
		});
		btnAufg3.setBounds(12, 86, 320, 25);
		contentPane.add(btnAufg3);

		JButton btnAufg4 = new JButton("Aufgabe 4: Punkte --> Note");
		btnAufg4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				aufg4();
			}
		});
		btnAufg4.setBounds(12, 123, 320, 25);
		contentPane.add(btnAufg4);

		JButton btnAufgabeMathetrainer = new JButton("Aufgabe 5: Mathe-Trainer");
		btnAufgabeMathetrainer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				aufg5();
			}
		});
		btnAufgabeMathetrainer.setBounds(12, 160, 320, 25);
		contentPane.add(btnAufgabeMathetrainer);

		JButton btnAufg6 = new JButton("Aufgabe 6: x² + p·x + q = 0 lösen");
		btnAufg6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				aufg6();
			}
		});
		btnAufg6.setBounds(12, 197, 320, 25);
		contentPane.add(btnAufg6);

		JButton btnAufg7 = new JButton("Aufgabe 7: Body Maß Index");
		btnAufg7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				aufg7();
			}
		});
		btnAufg7.setBounds(12, 234, 320, 25);
		contentPane.add(btnAufg7);

		JButton btnAufg8 = new JButton("Aufgabe 8: Niedersachsenticket");
		btnAufg8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				aufg8();
			}
		});
		btnAufg8.setBounds(12, 271, 320, 25);
		contentPane.add(btnAufg8);
	}

	public boolean zwischen1und100(int zahl) {
		if (zahl >= 1 && zahl <= 100) {
			return true;
		} else {
			return false;
		}

	}

	private void aufg1() {
		// Aufgabe 1
		String text = JOptionPane.showInputDialog("Gib eine Zahl ein");
		System.out.println(text);
		int wert = Integer.parseInt(text);

		if (zwischen1und100(wert) == true) {
			JOptionPane.showMessageDialog(this, wert + " liegt zwischen 1 und 100");
		} else {
			JOptionPane.showMessageDialog(this, wert + " liegt nicht zwischen 1 und 100");
		}
	}

	public boolean kleinerals10odergroeßerals20(double zahl) {
		if (zahl <= 10.0 || zahl >= 20.0) {
			return true;
		} else {
			return false;
		}
	}

	private void aufg2() {
		// Aufgabe 2
		String text = JOptionPane.showInputDialog("Gib eine Zahl ein");
		System.out.println(text);
		double wert = Double.parseDouble(text);

		if (kleinerals10odergroeßerals20(wert) == true) {
			JOptionPane.showMessageDialog(this, wert + " ist kleiner als 10 oder größer als 20");
		} else {
			JOptionPane.showMessageDialog(this, wert + " liegt zwischen 10 und 20");
		}

}
	private void aufg3() {
		// Aufgabe 3

	}

	private int zensurUmrechnung(int notenPunkte) {
		switch (notenPunkte) {
		case 0:
			return 6;
		case 1:
		case 2:
		case 3:
			return 5;
		case 4:
		case 5:
		case 6:
			return 4;
		case 7:
		case 8:
		case 9:
			return 3;
		case 10:
		case 11:
		case 12:
			return 2;
		case 13:
		case 14:
		case 15:
			return 1;
		default:
			return -1;
		}
	}

	private void aufg4() {
		// Aufgabe 4
		String eingabe;
		int punkte;
		int note;
		eingabe = JOptionPane.showInputDialog("Wie viele Notenpunkte hast du?");
		punkte = Integer.parseInt(eingabe);
		note = zensurUmrechnung(punkte);
		if (note != -1) {
			JOptionPane.showMessageDialog(this, "Das entspricht einer " + note);}
		else {
			JOptionPane.showMessageDialog(this, punkte + "? Das glaubst du doch selber nicht!");
			
		}

	}

	private void aufg5() {
		// Aufgabe 5

	}

	private void aufg6() {
		// Aufgabe 6

	}

	private void aufg7() {
		// Aufgabe 7

	}

	private void aufg8() {
		// Aufgabe 8

	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Dialoge frame = new Dialoge();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
